#include <stdio.h>
#include <string.h>
#include "phone.h"

int main(void)
{
	 int input;
     Phone book[MAX_NUM]; //사용자 정보를 저장할 구조체 배열
	 int size = 0; //저장된 user수
				    
	 //메뉴 선택
	 while (1){
	 printf("<<<<< Menu >>>>> \n");
	 printf("1. Register \t");
	 printf("2. Delete \t");
	 printf("3. Search \t");
	 printf("4. Exit \n");
	 printf("================\n");
	 printf("select menu: ");
	 scanf("%d", &input);
	
	 if (input == 1){
		 printf("\n[Register] \n");
		 Register(book, &size);
	 }
	 else if (input == 2){
	     printf("\n[Delete] \n");
		 Delete(book, &size);
	 }	
	 else if (input == 3){
		 printf("\n[Search] \n");
		 Search(book, &size);
	 }
	 else if (input == 4){
		  printf("\n[Exit] \n");
		  printf("===stop program=== \n\n");
		  return 0;
	 }
	 else
		 printf("\nError! ReTry! \n\n");
	 }
	 return 0;
}
