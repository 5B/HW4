OBJS = phone.o register.o delete.o search.o
all : main

phone : $(OBJS)
	gcc -o phone $(OBJS)
register.o : register.c
	gcc -c register.c
delete.o : delete.c
	gcc -c delete.c
search.o : search.c
	gcc -c search.c
phone.o : phone.c
	gcc -c phone.c
