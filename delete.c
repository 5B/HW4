#include<stdio.h>
#include<string.h>
#include "phone.h"

int Delete(Phone * ptr, int * num)
{
     char name[50];
     int i, j;
	 //유저 정보가 한개라도 남아있으면
	 if (*num > 0){
		 printf("Input Name: ");
		 scanf("%s", name);
		
		 for (i = 0; i < MAX_NUM; i++){
		 //문자열이므로 비교하기위해 strcmp사용
			 if (strcmp(name, ptr[i].name) == 0){
				 (*num)--;
                 printf(" === Data Deleted ===\n\n");
             //데이터가 가득 차지 않았다면
	         if (i != MAX_NUM - 1){
	             for (j = i; j < MAX_NUM; j++){
      	             //문자열이므로 strcpy를 사용하여 데이터 복사
		             strcpy(ptr[j].name, ptr[j + 1].name);
		             strcpy(ptr[j].number, ptr[j + 1].number);
		         }
			     //구조체 배열의 마지막을 NULL로 바꿈
				 *ptr[MAX_NUM - 1].name = NULL;
				 *ptr[MAX_NUM - 1].number = NULL;
			 }
             //데이터가 가득 찼다면
             else{
             //구조체 배열의 마지막을 NULL로 바꿈
	             *ptr[MAX_NUM - 1].name = NULL;
	             *ptr[MAX_NUM - 1].number = NULL;
			}
			return 0;
			}
		 }	
		 printf("Not Found \n\n");
		 return 0;
	 }

	 //저장된 유저 정보가 없다면
	 else{
	 	printf("  No Data \n\n");
		return 0;
	 }
}
